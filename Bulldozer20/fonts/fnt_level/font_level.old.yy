{
    "id": "2727f33a-a8e9-4555-b8de-14c5b3005e64",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "font_level",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Arial",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "03114c25-6ba0-42b8-9135-4fe9db50b9fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 18,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "958cabef-a410-4bad-bbf8-60c93d1bdb71",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 165,
                "y": 42
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "2971de30-865c-4547-b0ee-68cda0593e79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 18,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 158,
                "y": 42
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "2250505e-3cea-4387-9a62-f68f00e107df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 147,
                "y": 42
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "5b1f6b87-82b2-4737-b564-ee92e94fcfb1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 136,
                "y": 42
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "c903649f-3772-4e01-8abc-c58dd7a9286f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 18,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 120,
                "y": 42
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "5783f892-a0e4-4dc9-a901-12cb507176c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 107,
                "y": 42
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "875fe853-99e9-4b15-949d-e387c129fdaf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 18,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 102,
                "y": 42
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "64b3b5cf-7ddd-4070-b4c2-84a1ad7ee122",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 95,
                "y": 42
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "40d4cbf6-48c3-4b18-8367-0085ed2a8d7d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 88,
                "y": 42
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "c7ffb8ae-74d8-425c-bac2-d23dca8c15ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 18,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 170,
                "y": 42
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "901c3a51-79d8-496d-bc1a-b3f121400a59",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 77,
                "y": 42
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "a8236a29-20ea-4964-b359-89d1777f3176",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 61,
                "y": 42
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "a71b7d11-847a-42e0-b3ab-fb178d6b932f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 54,
                "y": 42
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "369feec0-9dcd-4a8b-a702-d9705f7a4db5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 49,
                "y": 42
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "47f3fff2-f95b-4fec-915e-c547a1516af5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 18,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 42,
                "y": 42
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "6cc120a1-a9e1-4405-ba67-6ac47bdb52da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 31,
                "y": 42
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "604a1b47-15ce-43c3-9bc9-d474d52d6e1c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 5,
                "x": 24,
                "y": 42
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "357bd89f-d435-4628-95cf-252c4afeff77",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 13,
                "y": 42
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "adf22ed1-6756-4252-b116-f2fa72489e52",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 42
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "3f3f065e-6025-4643-82f4-1269e74ffeb8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 240,
                "y": 22
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "e77e2163-e8d6-4aeb-82fe-58f1f7a8e094",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 66,
                "y": 42
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "fd802801-9c36-48ff-8aeb-c184b44ff4e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 178,
                "y": 42
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "f94d1311-f397-42a7-afd2-3d2c148da81f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 189,
                "y": 42
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "5a892ed2-3b11-4b88-9f75-62c3b1444bb8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 200,
                "y": 42
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "2f8b2ad7-d390-4e31-a4e4-f0e85804110f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 186,
                "y": 62
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "48beb8e8-0d20-4d1f-95a7-7ac482d58f26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 181,
                "y": 62
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "5b5a09f7-8de5-4a13-b959-66f6bd843d47",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 176,
                "y": 62
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "325ae104-c6ba-40f5-a6ea-c5574b53336f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 165,
                "y": 62
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "98640e6c-57d0-4a85-b1c3-acf3f998f0cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 154,
                "y": 62
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "941957ed-e4ea-4b1d-899b-ec8a8684435a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 143,
                "y": 62
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "8d350bb9-d4b0-4d1d-9405-7c898848503b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 132,
                "y": 62
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "51b46f73-4c1b-4508-9b32-f7b86ee080c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 18,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 114,
                "y": 62
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "0ed9fe51-a386-42f8-8cb2-ef915937237e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 18,
                "offset": -1,
                "shift": 11,
                "w": 12,
                "x": 100,
                "y": 62
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "4b736fb9-06ef-4289-8b69-a4d08c06ea3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 89,
                "y": 62
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "15fbf785-906b-47d8-aae8-deca50663f15",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 76,
                "y": 62
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "57f9c85b-291b-4ba6-b831-457817e435bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 18,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 64,
                "y": 62
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "1c696431-cbc3-4379-8d0a-acbf065c0f4a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 53,
                "y": 62
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "6542bcb9-8fe9-45c9-97f4-12672388be04",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 42,
                "y": 62
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "1ad2a2ff-40e2-4c0b-8b74-3758508d8dfa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 28,
                "y": 62
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "aa87a05e-521d-4f79-94c4-e850723d2737",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 18,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 16,
                "y": 62
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "70597229-8351-4b6a-897e-7c288b7fa489",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 11,
                "y": 62
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "cc308232-55c2-4b7d-a19d-c9b25adb9058",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 2,
                "y": 62
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "543633fa-7571-43ec-a219-070f46dfdfe5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 235,
                "y": 42
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "4e801a6c-4abf-49b5-afb3-4094c7cfdf4a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 225,
                "y": 42
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "ff54ec46-4cb1-4572-a668-db00bcd34351",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 18,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 211,
                "y": 42
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "c78f5aa0-6ca9-4aea-aa6b-5f30f8206a7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 18,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 228,
                "y": 22
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "cf18b07c-f698-4927-8cc0-fb54fb8feebf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 214,
                "y": 22
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "4db59e07-be4c-41e8-a8ac-8c40c81ddc01",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 203,
                "y": 22
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "a836fed9-91c2-4b88-a67f-02872cbec27b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 232,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "62e5dc39-fdb9-4399-ba1f-cd95f8bc4603",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 18,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 213,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "1d5b7279-01fb-43a0-826c-4a623efce183",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 201,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "54f45e49-2c9c-4412-8069-63530fd6ac5b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 189,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "648440fb-eb9f-40c2-bb94-f2424b938388",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 18,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 177,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "93fca4ee-7657-43f4-8a49-1fb0fff8f7c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 164,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "c0d8b46b-d23b-48ca-b484-9e234460bdc6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 18,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 147,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "9c9ea7a1-a7c9-4cda-a5df-25f7bf30830b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 134,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "9d3125b9-596c-407c-a6ec-de8dcd732814",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 121,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "92d40a31-c1cd-49d5-b977-d794fba051a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 109,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "7f79f64a-5b54-40cf-9b8b-2d2023cf1f58",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 4,
                "x": 226,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "cdd2ea63-9ee4-4eff-8b87-1b01e1ec967f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 18,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 102,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "cbbccf21-4315-4672-9ba5-23faa79386b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 18,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 89,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "bbf088d4-b75d-41d2-87c2-5f5dcbbb5897",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 79,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "460f74f1-3582-40a0-a4a2-ad8ececb958c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 18,
                "offset": -1,
                "shift": 9,
                "w": 11,
                "x": 66,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "1e4c52a9-b1eb-4115-82bd-086774edc10e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 60,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "8a519bac-b312-4254-baa9-450649426dab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 49,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "43112032-6128-4f48-b58b-7299b2842f6f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 39,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "5fa5bb98-6c7a-46e4-b4f5-fa867b0b6a4a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 29,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "254eb715-98d1-4661-87b6-981483e35edd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 19,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "8a78b404-6538-4556-a131-86a282ee7d02",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 8,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "207a2fe0-8872-42fc-b55f-dc48e2f16d24",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 18,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 95,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "2ee1e361-41c6-4558-9d37-168a380a4917",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 2,
                "y": 22
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "8b3b3d96-a1c1-4a30-a02e-8aac8d0b7b88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 97,
                "y": 22
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "dff278dd-d683-4593-a79b-5b99fa62e557",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 12,
                "y": 22
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "5aabac3b-16db-4d3c-9b06-9c66aba9b064",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 18,
                "offset": -1,
                "shift": 4,
                "w": 4,
                "x": 187,
                "y": 22
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "3caf69c3-57e3-4fed-8712-691ebbd78a78",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 18,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 178,
                "y": 22
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "670fb0dc-ed1f-4739-af88-b51e4d96482d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 174,
                "y": 22
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "a6c5a0f7-ca73-4045-b077-cce4b6ed19bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 18,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 160,
                "y": 22
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "458da3f8-5804-43b5-a6aa-5b020adbe04d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 151,
                "y": 22
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "ea75b240-0ada-4481-9ac4-2ca4b487fbec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 140,
                "y": 22
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "9529be30-43d1-4f6a-8818-045f23be38ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 130,
                "y": 22
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "302ff32f-ea6b-4abf-bb95-584823c09673",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 120,
                "y": 22
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "48f09f3a-da56-4d5d-baf9-bf5c534d2a3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 18,
                "offset": 1,
                "shift": 5,
                "w": 5,
                "x": 113,
                "y": 22
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "0a696f8e-c7b7-4b95-8ba6-b3cab4a80b52",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 193,
                "y": 22
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "baa82cac-ee6a-4a90-9b02-a9e02e03d553",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 18,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 106,
                "y": 22
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "535ab59e-911b-4323-b72a-2c65568d9f35",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 88,
                "y": 22
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "1c210e94-8d7f-4fe3-b3b0-4c8d8753d3c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 78,
                "y": 22
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "c4b5c53c-5a5f-4488-be90-12667652f4cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 64,
                "y": 22
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "5986424f-aa06-4dd7-8da0-bbd3db9403cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 54,
                "y": 22
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "8c16db33-97e3-4a75-804d-294d81fdb60c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 44,
                "y": 22
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "490dda01-05fc-4739-9db8-54c65e3e6b7a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 34,
                "y": 22
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "6184231f-566e-46fc-850b-9a0f42c55524",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 27,
                "y": 22
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "6aad55c3-01b6-460f-8379-9f7c57388ef3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 23,
                "y": 22
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "de7792a3-fc3e-48b0-a2e4-aef538398f61",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 16,
                "y": 22
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "242df576-0df7-431d-a37c-08edaaa83cea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 197,
                "y": 62
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "cf93b072-3a1d-4a28-818c-b48ffe4bdffa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 18,
                "offset": 3,
                "shift": 10,
                "w": 4,
                "x": 208,
                "y": 62
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "1cc7589a-e867-44b3-9f71-7684f6f2d02f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 65
        },
        {
            "id": "e5de6198-2206-47bc-ab3a-156ecda9b643",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 902
        },
        {
            "id": "5586e36d-435c-4e91-aa04-5fc264974ff3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 913
        },
        {
            "id": "06425e1f-ed40-4fb4-a6bb-819a7d2ef6ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 916
        },
        {
            "id": "0c8342f6-965b-4d4b-952f-3621beb3a432",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 923
        },
        {
            "id": "fc3ecdb4-d02f-4ef6-bfa6-4ccef8614303",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 49
        },
        {
            "id": "c8cb75a4-3282-49be-8e00-0d64a558f0f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 32
        },
        {
            "id": "6d7ee8cf-b115-4093-8e52-803997816d72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 84
        },
        {
            "id": "54d63086-b046-4484-bf39-ab220e415e78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 86
        },
        {
            "id": "1214aee8-9ba6-4b66-ba5d-99da992537bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 89
        },
        {
            "id": "fe2af623-62b2-4b0c-b94e-ab40be6d6aa4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 160
        },
        {
            "id": "7fdf30aa-8a1b-42e5-974a-3a416815a056",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8217
        },
        {
            "id": "266bd30d-b954-480f-9143-200de97a72d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 44
        },
        {
            "id": "b0c493a4-b250-4a70-a7b3-ca2972dfdb0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 46
        },
        {
            "id": "04f13ceb-2a5d-4ebc-9676-236f367f708d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 65
        },
        {
            "id": "884026b3-c1d6-449f-a01c-dba76ea57719",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 84
        },
        {
            "id": "3683bb4e-5f62-4ba6-8aed-07435a525f67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 86
        },
        {
            "id": "d15e229f-122a-40bc-9b48-3b179fa8ae10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 87
        },
        {
            "id": "ea0634d7-0ba3-479b-9ab4-68b6352db8f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 89
        },
        {
            "id": "dcae611e-1fd0-4e12-976c-ee470d8c1ac2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8217
        },
        {
            "id": "1b7bc457-3165-46e7-9dcd-5d49c6fc5780",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 44
        },
        {
            "id": "42c32fd8-e3e3-4b83-b50d-1ca99cf572c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 46
        },
        {
            "id": "d92ed7f1-7930-4355-be6d-9037a8cbbe1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 65
        },
        {
            "id": "5a1926bb-c589-4f60-bf1f-440371c0f966",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 44
        },
        {
            "id": "137454c7-00ca-43ae-ae70-aac466ca3e88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 45
        },
        {
            "id": "e99977c4-3f56-48f3-9b0c-9ccb701647c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 46
        },
        {
            "id": "020fa975-05f5-458a-9465-27e002f9dffb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 58
        },
        {
            "id": "f33d6af6-e0a0-49e0-b02e-062e3403bfee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 59
        },
        {
            "id": "c2ecf946-9f9e-4974-8084-34552c06d297",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 65
        },
        {
            "id": "c67b8281-e00b-45a3-be1e-2a6cd442922b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 97
        },
        {
            "id": "a79b1c8a-c1eb-45d5-999a-5127c3b3ad99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 99
        },
        {
            "id": "8685cd13-e2e1-4229-9bd7-9d38e6f75697",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 101
        },
        {
            "id": "ed3dea3d-b633-4b13-8468-99eae117df65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 111
        },
        {
            "id": "0a1530bc-345d-4593-909c-3d60b247fde6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 115
        },
        {
            "id": "58370796-f1c7-44db-9c41-66ffc267b6a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 119
        },
        {
            "id": "eb4af945-17f5-49b7-809a-fe0abeba1afb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 121
        },
        {
            "id": "b5140543-e7bb-4c67-b662-6db8a1e521e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 173
        },
        {
            "id": "52265a27-76df-426e-a4f0-4a6e97364f56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 894
        },
        {
            "id": "174ca818-0f07-4b77-9ed3-d3cc8a133ee8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 44
        },
        {
            "id": "d9f05396-5379-4fa7-9ef5-4c0da751a8db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 45
        },
        {
            "id": "4ad62897-b7fe-4b81-8ed8-9bb98d727193",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 46
        },
        {
            "id": "a9395e67-bb85-48a2-a065-9fb473eb97d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 65
        },
        {
            "id": "de6f2e32-d833-4837-af8d-c90c6412773e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 97
        },
        {
            "id": "2c98c08f-b30c-46b4-8b87-6d2db122f190",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 101
        },
        {
            "id": "47881f72-e794-46c4-b983-fb419afa51b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 111
        },
        {
            "id": "3b575a0e-4510-4e29-8a25-210e4ee7768e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 173
        },
        {
            "id": "21e57c91-85d6-4434-abfb-87c8f27feea3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 44
        },
        {
            "id": "ec6566f9-cfc1-4d8a-97e6-f900b9c60d1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 46
        },
        {
            "id": "f4f04747-ad1f-4856-a522-87cfa7fdd5f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 44
        },
        {
            "id": "fe42e79f-23fc-4fb7-8a3d-7a458c5413b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 45
        },
        {
            "id": "a3e87437-6156-491b-aa27-234e0a3ea8bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 46
        },
        {
            "id": "16ee0079-d0f2-456e-86cc-efd3b992c79f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 58
        },
        {
            "id": "730eaf57-d4a1-4d5d-8c4a-65d941b5ace3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 59
        },
        {
            "id": "00b9426b-217f-46ea-ae7f-f6588cc81b80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 65
        },
        {
            "id": "2595f768-eaf2-40dd-97a9-a4f8ecbf7680",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 97
        },
        {
            "id": "fb5677da-7564-4554-bf05-d828218e2588",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 101
        },
        {
            "id": "adf6f0a7-8446-46f9-a54f-9033b2d4241e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 111
        },
        {
            "id": "6f0593e3-3b10-46ac-a65c-2a9dc00df89e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 112
        },
        {
            "id": "057b4d03-edc2-41df-8cf3-bec8af37d6a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 113
        },
        {
            "id": "e1554453-e54a-4308-801d-8f6ae4bebff2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 117
        },
        {
            "id": "c68bf0f1-2f28-49b9-bcff-988dd0644f29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 118
        },
        {
            "id": "a8baea13-8ce4-4de9-bddd-93aba57e222c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 173
        },
        {
            "id": "055285e0-219d-41ca-864c-734435e4657d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 894
        },
        {
            "id": "d2bf1cc3-c43c-40f0-a005-a869f30ded4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 44
        },
        {
            "id": "9bd7c8bd-88b9-4f20-b796-50637e3fcb08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 46
        },
        {
            "id": "7fec48b2-a54d-469f-8f83-2e4b8ce81e53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 44
        },
        {
            "id": "c22f8108-f3a7-4625-a6b4-45aa0afdb4d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 46
        },
        {
            "id": "9bd10540-53ba-4d6e-8d10-eb6dfa9c6e9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 44
        },
        {
            "id": "ae0bfb20-941e-4166-ae47-e9a98507e110",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 46
        },
        {
            "id": "2f84d8de-1519-41d0-9e2c-ead4f2ace768",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 44
        },
        {
            "id": "87626841-5978-41f9-a318-faa92dd06df8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 46
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 12,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}