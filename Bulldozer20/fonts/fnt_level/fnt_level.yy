{
    "id": "2727f33a-a8e9-4555-b8de-14c5b3005e64",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_level",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 6,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Wickermanor",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "b830c680-50bc-4a3d-9e95-1d792642c70c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 123,
                "offset": 0,
                "shift": 21,
                "w": 21,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "88ef821f-1d50-4ac6-b484-4f28e7a16a7e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 123,
                "offset": 0,
                "shift": 16,
                "w": 11,
                "x": 188,
                "y": 377
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "4d9ed6ef-cddd-4f0f-9e8b-eb3e123cc7a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 123,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 159,
                "y": 377
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "62006f66-29f5-497a-8704-5d91f00d8858",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 123,
                "offset": 0,
                "shift": 53,
                "w": 48,
                "x": 109,
                "y": 377
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "0a278669-8019-4dc0-8125-e395aa1fc3c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 123,
                "offset": -3,
                "shift": 59,
                "w": 54,
                "x": 53,
                "y": 377
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "2c3f8075-5349-4119-8418-0c2f435443fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 123,
                "offset": 0,
                "shift": 54,
                "w": 49,
                "x": 2,
                "y": 377
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "410db9db-8fea-4d4c-a9fa-945aa5525766",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 123,
                "offset": -6,
                "shift": 64,
                "w": 68,
                "x": 911,
                "y": 252
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "eb0e5932-2a74-43e4-a7cf-e91b1948c86a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 123,
                "offset": 0,
                "shift": 16,
                "w": 11,
                "x": 898,
                "y": 252
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "c50109cf-fe6a-48b0-82ec-715843f07d33",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 123,
                "offset": 0,
                "shift": 27,
                "w": 22,
                "x": 874,
                "y": 252
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "2f060bb5-1127-4a64-9794-4eeca0ade6eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 123,
                "offset": 0,
                "shift": 27,
                "w": 22,
                "x": 850,
                "y": 252
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "f6bccdc7-78df-458a-8f3f-6805374ea01e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 123,
                "offset": 0,
                "shift": 53,
                "w": 48,
                "x": 201,
                "y": 377
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "22277f9f-01ef-446e-bf47-48179595a88e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 123,
                "offset": 0,
                "shift": 37,
                "w": 32,
                "x": 816,
                "y": 252
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "3106e52c-d94a-4889-a179-c7cab614bfeb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 123,
                "offset": 0,
                "shift": 16,
                "w": 11,
                "x": 747,
                "y": 252
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "183ce0f3-ea93-4bcb-a8ec-02283bf4f380",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 123,
                "offset": 0,
                "shift": 37,
                "w": 32,
                "x": 713,
                "y": 252
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "e806e227-f7c1-49e5-91d3-72d451603725",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 123,
                "offset": 0,
                "shift": 16,
                "w": 11,
                "x": 700,
                "y": 252
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "b209180d-8a3a-4a65-b9b0-401a359b52e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 123,
                "offset": 0,
                "shift": 53,
                "w": 48,
                "x": 650,
                "y": 252
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "fe946efb-f3f4-474d-841d-66156b07604a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 123,
                "offset": 0,
                "shift": 53,
                "w": 48,
                "x": 600,
                "y": 252
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "c495ae68-999b-44b0-9546-0ac75d7110f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 123,
                "offset": 0,
                "shift": 16,
                "w": 11,
                "x": 587,
                "y": 252
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "d8daaa19-9ade-4a35-8b40-36231cd1537d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 123,
                "offset": -3,
                "shift": 53,
                "w": 54,
                "x": 531,
                "y": 252
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "9943d18e-f95a-43b2-ba93-491d964c941e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 123,
                "offset": -3,
                "shift": 53,
                "w": 56,
                "x": 473,
                "y": 252
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "9fa78174-62f6-4027-bf9a-0c8dfa21d773",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 123,
                "offset": 0,
                "shift": 53,
                "w": 48,
                "x": 423,
                "y": 252
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "ab0377b5-7b9e-4530-8d1e-222bb29f3a94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 123,
                "offset": -3,
                "shift": 53,
                "w": 54,
                "x": 760,
                "y": 252
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "0927a571-72d6-43c1-963b-3b513d33ebf1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 123,
                "offset": 0,
                "shift": 53,
                "w": 51,
                "x": 251,
                "y": 377
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "404132b1-a6aa-4586-8686-81a6feb44c49",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 123,
                "offset": -3,
                "shift": 54,
                "w": 52,
                "x": 304,
                "y": 377
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "ceab52ce-9096-431d-b8b1-055d3502771a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 123,
                "offset": 0,
                "shift": 53,
                "w": 48,
                "x": 358,
                "y": 377
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "6f8bae87-e9b3-4b8e-8227-3663f9a5d337",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 123,
                "offset": -3,
                "shift": 53,
                "w": 51,
                "x": 365,
                "y": 502
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "5547e55f-9047-4624-8371-95d0955e43c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 123,
                "offset": 0,
                "shift": 16,
                "w": 11,
                "x": 352,
                "y": 502
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "4caf34c8-f233-49db-baee-be7f5999fdd2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 123,
                "offset": 0,
                "shift": 16,
                "w": 11,
                "x": 339,
                "y": 502
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "164851ec-8419-4176-aebd-8e8c1b2735b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 123,
                "offset": -3,
                "shift": 32,
                "w": 30,
                "x": 307,
                "y": 502
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "9b220a39-cb4d-44f1-bf7f-174d2154d3bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 123,
                "offset": 0,
                "shift": 37,
                "w": 32,
                "x": 273,
                "y": 502
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "f3497549-912c-40c7-aae2-7e07a421ee7f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 123,
                "offset": 0,
                "shift": 32,
                "w": 30,
                "x": 241,
                "y": 502
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "b23d7e5f-12a5-46a9-ac56-981aa2e4b876",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 123,
                "offset": 0,
                "shift": 53,
                "w": 48,
                "x": 191,
                "y": 502
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "c17a1969-645a-4bb3-a330-0f62e68ac153",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 123,
                "offset": 0,
                "shift": 80,
                "w": 75,
                "x": 114,
                "y": 502
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "5c0cf8a3-d9ac-4ad0-99d2-595a05f48fae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 123,
                "offset": -6,
                "shift": 48,
                "w": 49,
                "x": 63,
                "y": 502
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "e7a5e4eb-695f-4094-9b8b-d7b5881605d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 123,
                "offset": -11,
                "shift": 53,
                "w": 59,
                "x": 2,
                "y": 502
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "4a924ae3-04b6-4561-82a9-bbad1c52ed78",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 123,
                "offset": 0,
                "shift": 53,
                "w": 59,
                "x": 921,
                "y": 377
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "80a4b952-bee3-4e81-b042-90669a324495",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 123,
                "offset": -11,
                "shift": 53,
                "w": 59,
                "x": 860,
                "y": 377
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "56ff9d10-1428-472e-b95a-4fb55f89d0d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 123,
                "offset": -6,
                "shift": 53,
                "w": 57,
                "x": 801,
                "y": 377
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "91a0f51c-ae70-4824-81a5-28fecb1dd0d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 123,
                "offset": -6,
                "shift": 53,
                "w": 57,
                "x": 742,
                "y": 377
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "b65ab690-4cc6-4272-b972-46f64d74b3fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 123,
                "offset": 0,
                "shift": 53,
                "w": 59,
                "x": 681,
                "y": 377
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "e7aff548-0938-49d2-a6d7-cc43ea6eafc7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 123,
                "offset": 0,
                "shift": 53,
                "w": 48,
                "x": 631,
                "y": 377
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "66bf5a9c-2c94-4f39-8d58-2f51129fb493",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 123,
                "offset": 0,
                "shift": 16,
                "w": 11,
                "x": 618,
                "y": 377
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "a5a749b6-05cb-4d13-98ea-f7f1717594b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 123,
                "offset": 0,
                "shift": 43,
                "w": 38,
                "x": 578,
                "y": 377
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "a61f70ae-73dd-42ed-9c6f-4047e94f3f95",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 123,
                "offset": 0,
                "shift": 53,
                "w": 48,
                "x": 528,
                "y": 377
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "8c5000cf-1a78-4f6b-8033-2c57149f6ea7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 123,
                "offset": 0,
                "shift": 53,
                "w": 51,
                "x": 475,
                "y": 377
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "1831e62c-42ec-4a8c-a643-aa95a7389d16",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 123,
                "offset": -6,
                "shift": 64,
                "w": 65,
                "x": 408,
                "y": 377
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "a09c464f-23bc-44d8-b456-ed76a322d42e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 123,
                "offset": 0,
                "shift": 53,
                "w": 48,
                "x": 373,
                "y": 252
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "e3e5aa6a-28a1-4ded-9047-097a97e1bc4a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 123,
                "offset": 0,
                "shift": 53,
                "w": 48,
                "x": 323,
                "y": 252
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "af326929-9b00-484f-b1c2-43515166bd07",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 123,
                "offset": -11,
                "shift": 53,
                "w": 59,
                "x": 262,
                "y": 252
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "1361f5dd-65b6-460c-b332-18685db4427e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 123,
                "offset": 0,
                "shift": 53,
                "w": 48,
                "x": 87,
                "y": 127
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "30bf21ca-1710-4b29-973e-455c22133e71",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 123,
                "offset": -11,
                "shift": 53,
                "w": 59,
                "x": 2,
                "y": 127
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "aac7872f-beb6-4129-a7d2-780da1e5a446",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 123,
                "offset": -3,
                "shift": 53,
                "w": 54,
                "x": 916,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "381da869-8dbd-48d0-98df-5411591a74fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 123,
                "offset": -6,
                "shift": 48,
                "w": 54,
                "x": 860,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "4b76b914-8051-473a-ac2d-e7306a5b0635",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 123,
                "offset": 0,
                "shift": 53,
                "w": 48,
                "x": 810,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "567d506d-0d03-4ab7-8d62-ea114e1d90fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 123,
                "offset": -6,
                "shift": 48,
                "w": 49,
                "x": 759,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "60d15260-19cd-4cf0-9920-5a4a7acfbe4e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 123,
                "offset": -6,
                "shift": 64,
                "w": 65,
                "x": 692,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "d0357f29-145e-4fd5-ac46-7412576df518",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 123,
                "offset": 0,
                "shift": 53,
                "w": 48,
                "x": 642,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "caf45e43-6a44-440f-8bfa-29ff55f5c184",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 123,
                "offset": 0,
                "shift": 53,
                "w": 48,
                "x": 592,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "9a958186-73b5-473c-a46b-d0eac0e8b2d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 123,
                "offset": -3,
                "shift": 53,
                "w": 53,
                "x": 537,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "cb0de4b2-f305-4a52-8c7b-f2c813d4fb27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 123,
                "offset": 0,
                "shift": 27,
                "w": 22,
                "x": 63,
                "y": 127
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "ff456435-3047-4b14-a187-60f4c8e6c4ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 123,
                "offset": 0,
                "shift": 53,
                "w": 48,
                "x": 487,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "c624c91e-a13b-4cba-9c4b-c1794bbf9b7f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 123,
                "offset": 0,
                "shift": 27,
                "w": 22,
                "x": 404,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "b2442963-0c14-45f2-8f7c-11990bf7799f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 123,
                "offset": 0,
                "shift": 26,
                "w": 21,
                "x": 381,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "ac1dd5b4-fd41-41a1-bb53-7af4f3601e9d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 123,
                "offset": 0,
                "shift": 53,
                "w": 48,
                "x": 331,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "4e6656be-6d7e-4d82-b1c5-7e77ca708d4b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 123,
                "offset": 0,
                "shift": 16,
                "w": 11,
                "x": 318,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "ab889ea6-a025-4c2f-b64a-63d7ec30b302",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 123,
                "offset": -6,
                "shift": 48,
                "w": 49,
                "x": 267,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "ab94aa53-59da-4682-b13a-19dfb130e74a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 123,
                "offset": -11,
                "shift": 53,
                "w": 59,
                "x": 206,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "538edaf8-67c3-49ae-a07f-1ca2fbaf00f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 123,
                "offset": 0,
                "shift": 53,
                "w": 59,
                "x": 145,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "41689b52-35ea-4953-ade0-821540b18e04",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 123,
                "offset": -11,
                "shift": 53,
                "w": 59,
                "x": 84,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "40e17abf-843a-48c4-ba35-8606fe7fc6da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 123,
                "offset": -6,
                "shift": 53,
                "w": 57,
                "x": 25,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "92a125e0-8f3d-4cb8-9a10-6cdf535ea826",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 123,
                "offset": -6,
                "shift": 53,
                "w": 57,
                "x": 428,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "9a722203-d5de-4993-88f0-8668a21c803b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 123,
                "offset": 0,
                "shift": 53,
                "w": 59,
                "x": 137,
                "y": 127
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "a579781d-7839-4cca-8ba0-3c8a47dfca5a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 123,
                "offset": 0,
                "shift": 53,
                "w": 48,
                "x": 595,
                "y": 127
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "5e4b9002-7723-49e6-bf1a-1071125bae84",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 123,
                "offset": 0,
                "shift": 16,
                "w": 11,
                "x": 198,
                "y": 127
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "103f2392-9441-4813-92e0-8ebe63c9b9a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 123,
                "offset": 0,
                "shift": 37,
                "w": 32,
                "x": 172,
                "y": 252
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "3de4ce7c-896a-4128-8271-18d16a751f77",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 123,
                "offset": 0,
                "shift": 53,
                "w": 48,
                "x": 122,
                "y": 252
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "e411951c-da13-4094-b7e3-891431996b40",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 123,
                "offset": 0,
                "shift": 53,
                "w": 51,
                "x": 69,
                "y": 252
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "63c43088-4341-4494-bc69-38568c1ce5bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 123,
                "offset": -6,
                "shift": 64,
                "w": 65,
                "x": 2,
                "y": 252
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "f2d4b2fd-7dd4-4cd3-8117-bf94e3767c70",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 123,
                "offset": 0,
                "shift": 53,
                "w": 48,
                "x": 923,
                "y": 127
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "553383ed-9935-4cd7-ad91-4563b86fbf43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 123,
                "offset": 0,
                "shift": 53,
                "w": 48,
                "x": 873,
                "y": 127
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "096cf4c3-daf1-4752-a7e8-173be2393b67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 123,
                "offset": -11,
                "shift": 53,
                "w": 59,
                "x": 812,
                "y": 127
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "7c124f04-09bd-48e0-a10a-cb6a950c0009",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 123,
                "offset": 0,
                "shift": 53,
                "w": 48,
                "x": 762,
                "y": 127
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "efdb79c8-630a-459d-9de9-1001397a6d96",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 123,
                "offset": -11,
                "shift": 53,
                "w": 59,
                "x": 701,
                "y": 127
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "020c6e8c-be0f-4f87-a9f9-8894d6b7b50c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 123,
                "offset": -3,
                "shift": 53,
                "w": 54,
                "x": 206,
                "y": 252
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "3743d8c3-d817-4b74-887e-6a75b10ff28a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 123,
                "offset": -6,
                "shift": 48,
                "w": 54,
                "x": 645,
                "y": 127
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "c43b72c2-1d6a-464d-a5cc-221a919c5b58",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 123,
                "offset": 0,
                "shift": 53,
                "w": 48,
                "x": 545,
                "y": 127
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "37fe590b-cea6-47d1-a4e4-6a0a2ee5719d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 123,
                "offset": -6,
                "shift": 48,
                "w": 49,
                "x": 494,
                "y": 127
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "1e829790-1636-4611-abbd-c2c04fa3d8e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 123,
                "offset": -6,
                "shift": 64,
                "w": 65,
                "x": 427,
                "y": 127
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "4d487771-5c1b-4174-8682-453cd4b50993",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 123,
                "offset": 0,
                "shift": 53,
                "w": 48,
                "x": 377,
                "y": 127
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "1fab4428-c299-413f-a71d-1dc4d26f603d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 123,
                "offset": 0,
                "shift": 53,
                "w": 48,
                "x": 327,
                "y": 127
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "05377844-5180-4668-aebc-3f7062a497f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 123,
                "offset": -3,
                "shift": 53,
                "w": 53,
                "x": 272,
                "y": 127
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "e08e88db-229d-4134-8d68-daa8ce5fd72b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 123,
                "offset": 0,
                "shift": 27,
                "w": 22,
                "x": 248,
                "y": 127
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "0b9a1ccb-2c80-4a74-a846-d7ab5878a97b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 123,
                "offset": 0,
                "shift": 16,
                "w": 11,
                "x": 235,
                "y": 127
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "dac298a7-d116-497c-b70d-f91c65979612",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 123,
                "offset": 0,
                "shift": 27,
                "w": 22,
                "x": 211,
                "y": 127
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "9a15dbd3-45a3-4aa8-bd18-9b2a893815c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 123,
                "offset": 12,
                "shift": 103,
                "w": 78,
                "x": 418,
                "y": 502
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "7c8be9b6-0c8c-4e2c-a7e8-d985415845ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 123,
                "offset": 32,
                "shift": 103,
                "w": 38,
                "x": 498,
                "y": 502
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 128,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}