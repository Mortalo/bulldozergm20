{
    "id": "95215348-7c4b-4c82-b7b1-4f150fab2ae0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_building_trash_3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 159,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 32,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "adcbebe4-be41-4aa8-bc52-62b0aeae009a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95215348-7c4b-4c82-b7b1-4f150fab2ae0",
            "compositeImage": {
                "id": "af9b21a4-77f7-4bc9-9f5b-4556f84018b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "adcbebe4-be41-4aa8-bc52-62b0aeae009a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1cd0993d-8a9c-4735-a5ee-5db7b3476b31",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "adcbebe4-be41-4aa8-bc52-62b0aeae009a",
                    "LayerId": "da810aac-754a-4254-89a9-b540943f3c3a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 160,
    "layers": [
        {
            "id": "da810aac-754a-4254-89a9-b540943f3c3a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "95215348-7c4b-4c82-b7b1-4f150fab2ae0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 96
}