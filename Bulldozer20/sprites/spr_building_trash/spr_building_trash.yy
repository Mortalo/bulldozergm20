{
    "id": "6d7b7064-86bd-48c5-b011-0ab6e244aa90",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_building_trash",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 159,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 32,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f6b131c0-c386-44a9-88e3-8f2504aa2c87",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6d7b7064-86bd-48c5-b011-0ab6e244aa90",
            "compositeImage": {
                "id": "b6615ad4-3587-4eb5-bd05-36ed13685abd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6b131c0-c386-44a9-88e3-8f2504aa2c87",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e88c2eb8-13c6-4330-a684-ee1e7a143672",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6b131c0-c386-44a9-88e3-8f2504aa2c87",
                    "LayerId": "1f799db1-e1cf-4e8d-85fb-ce63046ef9ef"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 160,
    "layers": [
        {
            "id": "1f799db1-e1cf-4e8d-85fb-ce63046ef9ef",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6d7b7064-86bd-48c5-b011-0ab6e244aa90",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 96
}