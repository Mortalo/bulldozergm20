{
    "id": "f701565f-e1e8-4f7b-b630-d8ceb7043fef",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "tileset",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 128,
    "bbox_right": 511,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f4e4bb57-ddd5-4b79-a853-3c87e7473cbc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f701565f-e1e8-4f7b-b630-d8ceb7043fef",
            "compositeImage": {
                "id": "a6562a8e-1e36-43fe-b877-ac0338b95afc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f4e4bb57-ddd5-4b79-a853-3c87e7473cbc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a50a7277-1514-493c-b0fd-3912b9a121ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4e4bb57-ddd5-4b79-a853-3c87e7473cbc",
                    "LayerId": "13f48ec5-5fdb-4288-9cc6-514dfa200dfc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "13f48ec5-5fdb-4288-9cc6-514dfa200dfc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f701565f-e1e8-4f7b-b630-d8ceb7043fef",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 512,
    "xorig": 0,
    "yorig": 0
}