{
    "id": "0484423d-a6de-4daa-b94b-cc7aba575cd8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_start",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 159,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 32,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a8668914-966c-4932-9697-8390f2aba04e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0484423d-a6de-4daa-b94b-cc7aba575cd8",
            "compositeImage": {
                "id": "8877ca60-ca48-4316-9713-8fc2be7090aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8668914-966c-4932-9697-8390f2aba04e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3863986e-42ce-4368-a51f-b1310d17d501",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8668914-966c-4932-9697-8390f2aba04e",
                    "LayerId": "f0ba2931-5de5-4500-b810-cc1810d05f01"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 160,
    "layers": [
        {
            "id": "f0ba2931-5de5-4500-b810-cc1810d05f01",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0484423d-a6de-4daa-b94b-cc7aba575cd8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 96
}