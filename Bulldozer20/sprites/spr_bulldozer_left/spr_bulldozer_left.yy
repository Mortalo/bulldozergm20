{
    "id": "67ad6d1c-f0da-41c3-aea5-e0d5756bac19",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bulldozer_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 151,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 32,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6977a59b-55e0-46e4-92f4-3b2c68efa8a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "67ad6d1c-f0da-41c3-aea5-e0d5756bac19",
            "compositeImage": {
                "id": "63115a80-5b50-428b-be12-44b2048b45ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6977a59b-55e0-46e4-92f4-3b2c68efa8a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39abe424-255c-4587-bf4b-aa4d5ec59872",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6977a59b-55e0-46e4-92f4-3b2c68efa8a0",
                    "LayerId": "bb70182b-8a4b-4c8d-a7eb-5c3a136449e2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 160,
    "layers": [
        {
            "id": "bb70182b-8a4b-4c8d-a7eb-5c3a136449e2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "67ad6d1c-f0da-41c3-aea5-e0d5756bac19",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 96
}