{
    "id": "7c707498-fc08-4c5e-b477-2d5dbcca4994",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_end",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 137,
    "bbox_left": 7,
    "bbox_right": 491,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a794f5bf-1d11-4822-ad3a-9a1245da0215",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c707498-fc08-4c5e-b477-2d5dbcca4994",
            "compositeImage": {
                "id": "89e9f76c-a49b-45bd-a8ab-f91ccc0de689",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a794f5bf-1d11-4822-ad3a-9a1245da0215",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ba8f44e-437d-42da-8fa6-fe1e2bd12c09",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a794f5bf-1d11-4822-ad3a-9a1245da0215",
                    "LayerId": "93a5ef6c-f7bb-4387-b4da-b3005c6ef1ee"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 160,
    "layers": [
        {
            "id": "93a5ef6c-f7bb-4387-b4da-b3005c6ef1ee",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7c707498-fc08-4c5e-b477-2d5dbcca4994",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 500,
    "xorig": 250,
    "yorig": 80
}