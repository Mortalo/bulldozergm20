{
    "id": "b1113ed6-dc22-45b1-8b96-5c370591c001",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_green",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 128,
    "bbox_right": 639,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bcabd5c4-8572-4520-b32c-f9253f68dc77",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1113ed6-dc22-45b1-8b96-5c370591c001",
            "compositeImage": {
                "id": "befc42a5-e1a7-4200-b19c-e5eb349ec65f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bcabd5c4-8572-4520-b32c-f9253f68dc77",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4090a155-910e-44ca-8474-d6c05d0a0211",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bcabd5c4-8572-4520-b32c-f9253f68dc77",
                    "LayerId": "d723d2af-ddd9-48b7-bb01-e634be262394"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "d723d2af-ddd9-48b7-bb01-e634be262394",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b1113ed6-dc22-45b1-8b96-5c370591c001",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 0,
    "yorig": 0
}