{
    "id": "267183e9-201f-4b5f-aff3-e50f22dca6e2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_trexwww_200",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 112,
    "bbox_left": 3,
    "bbox_right": 397,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "705071cb-d4a0-4084-88f7-2c465090ad3e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "267183e9-201f-4b5f-aff3-e50f22dca6e2",
            "compositeImage": {
                "id": "76f1227f-df9a-4cb6-ac44-13d87276bd2a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "705071cb-d4a0-4084-88f7-2c465090ad3e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7472b43-2d0e-4995-89b4-6ef82e264e4f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "705071cb-d4a0-4084-88f7-2c465090ad3e",
                    "LayerId": "34bfd29c-e3cb-4972-88ea-817a1607d27e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 120,
    "layers": [
        {
            "id": "34bfd29c-e3cb-4972-88ea-817a1607d27e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "267183e9-201f-4b5f-aff3-e50f22dca6e2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 406,
    "xorig": 0,
    "yorig": 0
}