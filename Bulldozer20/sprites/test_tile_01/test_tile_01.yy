{
    "id": "266917e5-bd9c-40a9-b5af-4c68a41ee68c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "test_tile_01",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 128,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "acfcc7a0-bbe5-4855-8a0b-5fb63a85d01c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "266917e5-bd9c-40a9-b5af-4c68a41ee68c",
            "compositeImage": {
                "id": "7b2933cc-cd56-424e-878c-4a853838896d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "acfcc7a0-bbe5-4855-8a0b-5fb63a85d01c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "021d535d-4046-4066-9edb-ca1aab6e89df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "acfcc7a0-bbe5-4855-8a0b-5fb63a85d01c",
                    "LayerId": "3ee280af-0522-46cc-8aba-e9e1f77ebf19"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "3ee280af-0522-46cc-8aba-e9e1f77ebf19",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "266917e5-bd9c-40a9-b5af-4c68a41ee68c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}