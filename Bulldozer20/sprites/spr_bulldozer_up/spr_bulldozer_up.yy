{
    "id": "bfb4e446-2d32-4c64-95e8-cc221a311bc2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bulldozer_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 151,
    "bbox_left": 4,
    "bbox_right": 123,
    "bbox_top": 32,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "472b7d0a-b9e1-4e0f-9d29-6b059189ea8a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bfb4e446-2d32-4c64-95e8-cc221a311bc2",
            "compositeImage": {
                "id": "580b6f38-1a24-44c5-8ad3-2753ce38df0c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "472b7d0a-b9e1-4e0f-9d29-6b059189ea8a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f6c6aa4-8395-4f5b-8ab1-17594ad6868d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "472b7d0a-b9e1-4e0f-9d29-6b059189ea8a",
                    "LayerId": "21b13d01-9756-4b07-afcd-3163d5a4e71b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 160,
    "layers": [
        {
            "id": "21b13d01-9756-4b07-afcd-3163d5a4e71b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bfb4e446-2d32-4c64-95e8-cc221a311bc2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 96
}