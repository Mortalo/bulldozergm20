{
    "id": "af255107-1e25-4b51-b83b-82788935b187",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_building_trash_2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 159,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 23,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3dad55a7-8f1f-4a1a-a53d-9d3381d0d07a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "af255107-1e25-4b51-b83b-82788935b187",
            "compositeImage": {
                "id": "80573610-b05a-41c1-b4c9-398f6e9293eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3dad55a7-8f1f-4a1a-a53d-9d3381d0d07a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7427e13e-91a1-428a-bfd3-375fde12c6c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3dad55a7-8f1f-4a1a-a53d-9d3381d0d07a",
                    "LayerId": "f79dcb77-68e2-4bcc-bf65-db40c1a98e44"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 160,
    "layers": [
        {
            "id": "f79dcb77-68e2-4bcc-bf65-db40c1a98e44",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "af255107-1e25-4b51-b83b-82788935b187",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 96
}