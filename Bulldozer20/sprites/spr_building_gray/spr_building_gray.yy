{
    "id": "83e4b0f2-89b8-4b2f-9bde-c44e353a7953",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_building_gray",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 159,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 33,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "01ba8360-993c-4b03-9264-4062ccfaa982",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "83e4b0f2-89b8-4b2f-9bde-c44e353a7953",
            "compositeImage": {
                "id": "e423472d-447e-4680-b0c4-c73dcbc3168a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01ba8360-993c-4b03-9264-4062ccfaa982",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb9d1550-ade3-47d9-b826-83352e6907ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01ba8360-993c-4b03-9264-4062ccfaa982",
                    "LayerId": "f7fd5865-2c40-42f1-a5cf-7ca5ce796387"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 160,
    "layers": [
        {
            "id": "f7fd5865-2c40-42f1-a5cf-7ca5ce796387",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "83e4b0f2-89b8-4b2f-9bde-c44e353a7953",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 96
}