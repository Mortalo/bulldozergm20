{
    "id": "1c450bdb-9b9f-4711-91d8-424aaf3666d0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_credits",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 134,
    "bbox_left": 13,
    "bbox_right": 668,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "582cc883-72e4-43fc-96be-6ac2c4b21da0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c450bdb-9b9f-4711-91d8-424aaf3666d0",
            "compositeImage": {
                "id": "c16cf96c-8451-4951-969a-b671d2cabe23",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "582cc883-72e4-43fc-96be-6ac2c4b21da0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2bda7587-505f-4bc9-8624-ca25e7e3e431",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "582cc883-72e4-43fc-96be-6ac2c4b21da0",
                    "LayerId": "3de4a8a8-ba8b-43ee-9736-a215955561ef"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 400,
    "layers": [
        {
            "id": "3de4a8a8-ba8b-43ee-9736-a215955561ef",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1c450bdb-9b9f-4711-91d8-424aaf3666d0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1000,
    "xorig": 670,
    "yorig": 159
}