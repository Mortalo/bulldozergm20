{
    "id": "fa5d1802-3005-4e40-84df-2529bf8c269b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "tileset_v3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 128,
    "bbox_right": 639,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0be65e78-11c0-442b-8871-ec1ac6f3542a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa5d1802-3005-4e40-84df-2529bf8c269b",
            "compositeImage": {
                "id": "1782f1d6-6396-4e75-b292-142b0590f23c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0be65e78-11c0-442b-8871-ec1ac6f3542a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "109285a1-f422-4d3b-b35e-685e58544c1e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0be65e78-11c0-442b-8871-ec1ac6f3542a",
                    "LayerId": "ef5d5349-86f4-403b-b82a-08b7feb916b0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "ef5d5349-86f4-403b-b82a-08b7feb916b0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fa5d1802-3005-4e40-84df-2529bf8c269b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 0,
    "yorig": 0
}