{
    "id": "0bd3c802-0216-4db3-97e1-2e502aefd5f4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bulldozer",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 159,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 32,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ea5f405a-840d-4431-9425-8a20e3260d0f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bd3c802-0216-4db3-97e1-2e502aefd5f4",
            "compositeImage": {
                "id": "75ae13e0-2f80-4f66-a094-70f556541e2c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea5f405a-840d-4431-9425-8a20e3260d0f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "862b3420-0761-4c0a-b31b-9e40e7b6c26f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea5f405a-840d-4431-9425-8a20e3260d0f",
                    "LayerId": "cc65775c-cd5f-48eb-901c-59e372c23561"
                }
            ]
        },
        {
            "id": "740240bc-d971-4e90-9db2-87b38f115ffe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bd3c802-0216-4db3-97e1-2e502aefd5f4",
            "compositeImage": {
                "id": "c16c7d88-72d7-4c19-99af-9344560cc361",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "740240bc-d971-4e90-9db2-87b38f115ffe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "137a0518-0421-4da9-a2b3-049039242eca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "740240bc-d971-4e90-9db2-87b38f115ffe",
                    "LayerId": "cc65775c-cd5f-48eb-901c-59e372c23561"
                }
            ]
        },
        {
            "id": "4a93bb50-cd47-41d0-9a7a-dd29790a009b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bd3c802-0216-4db3-97e1-2e502aefd5f4",
            "compositeImage": {
                "id": "62c27877-6b93-47a8-9920-82ac0e1a6747",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a93bb50-cd47-41d0-9a7a-dd29790a009b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a95b99e-1e34-4f4c-b0d8-8b69502d59fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a93bb50-cd47-41d0-9a7a-dd29790a009b",
                    "LayerId": "cc65775c-cd5f-48eb-901c-59e372c23561"
                }
            ]
        },
        {
            "id": "8d0e6de5-54ca-4627-b99e-7e89248c7124",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bd3c802-0216-4db3-97e1-2e502aefd5f4",
            "compositeImage": {
                "id": "8db1d999-81fa-4d63-aab3-557b3d9ccc97",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d0e6de5-54ca-4627-b99e-7e89248c7124",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11a19d3f-7393-44ee-987f-cf0a708c8e50",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d0e6de5-54ca-4627-b99e-7e89248c7124",
                    "LayerId": "cc65775c-cd5f-48eb-901c-59e372c23561"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 160,
    "layers": [
        {
            "id": "cc65775c-cd5f-48eb-901c-59e372c23561",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0bd3c802-0216-4db3-97e1-2e502aefd5f4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 96
}