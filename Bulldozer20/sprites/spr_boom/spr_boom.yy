{
    "id": "1fbbe74d-5936-47a5-ac27-b677488dc985",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_boom",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 246,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cbf0d774-724c-4653-8e69-1ae38326da8b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1fbbe74d-5936-47a5-ac27-b677488dc985",
            "compositeImage": {
                "id": "577de907-79ad-4734-9b4b-d4f81934d8c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cbf0d774-724c-4653-8e69-1ae38326da8b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e279492f-34cd-4450-91bf-76132abdf70f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cbf0d774-724c-4653-8e69-1ae38326da8b",
                    "LayerId": "fcf34e9a-4d00-4249-bae5-19469fea6832"
                }
            ]
        },
        {
            "id": "e99fc8ee-41f0-422a-99a9-f187f4883590",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1fbbe74d-5936-47a5-ac27-b677488dc985",
            "compositeImage": {
                "id": "037c4626-3dfa-4980-a79b-4a5808fc43a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e99fc8ee-41f0-422a-99a9-f187f4883590",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff9bf6c0-c593-4d65-a3e0-234c96faadc5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e99fc8ee-41f0-422a-99a9-f187f4883590",
                    "LayerId": "fcf34e9a-4d00-4249-bae5-19469fea6832"
                }
            ]
        },
        {
            "id": "80faac71-7e67-43b8-ab80-c7b7fb2521d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1fbbe74d-5936-47a5-ac27-b677488dc985",
            "compositeImage": {
                "id": "1d84c597-6cf5-4431-aa18-bf4dc9114f3f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80faac71-7e67-43b8-ab80-c7b7fb2521d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d2f47694-78ec-440f-81ce-c7944960a953",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80faac71-7e67-43b8-ab80-c7b7fb2521d9",
                    "LayerId": "fcf34e9a-4d00-4249-bae5-19469fea6832"
                }
            ]
        },
        {
            "id": "edb33b2f-40f9-42e7-a87c-056286b7375e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1fbbe74d-5936-47a5-ac27-b677488dc985",
            "compositeImage": {
                "id": "ceba236c-92aa-4538-8228-3b0713b84857",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "edb33b2f-40f9-42e7-a87c-056286b7375e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b050674-3577-4172-9772-85b78bd62d5e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "edb33b2f-40f9-42e7-a87c-056286b7375e",
                    "LayerId": "fcf34e9a-4d00-4249-bae5-19469fea6832"
                }
            ]
        },
        {
            "id": "b6d39565-7aa7-46cd-b173-e88333e541d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1fbbe74d-5936-47a5-ac27-b677488dc985",
            "compositeImage": {
                "id": "23808393-cd1c-4918-957a-ebce4a84644b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6d39565-7aa7-46cd-b173-e88333e541d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b345d0d1-c78c-449b-9c8b-f8c100461e48",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6d39565-7aa7-46cd-b173-e88333e541d9",
                    "LayerId": "fcf34e9a-4d00-4249-bae5-19469fea6832"
                }
            ]
        },
        {
            "id": "585a268f-27f8-4358-993e-f9265c1700fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1fbbe74d-5936-47a5-ac27-b677488dc985",
            "compositeImage": {
                "id": "76520d40-fcd4-472b-9d7c-4df65ca3c04d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "585a268f-27f8-4358-993e-f9265c1700fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08585eef-7c52-41cc-a3b9-60fde2c1878d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "585a268f-27f8-4358-993e-f9265c1700fe",
                    "LayerId": "fcf34e9a-4d00-4249-bae5-19469fea6832"
                }
            ]
        },
        {
            "id": "57dd2b8b-b56e-4a65-8d38-47c4eab999c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1fbbe74d-5936-47a5-ac27-b677488dc985",
            "compositeImage": {
                "id": "8628cbef-17e3-4325-8333-193659a1f0a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57dd2b8b-b56e-4a65-8d38-47c4eab999c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00cbda4f-3dd2-459d-9e64-61b8d1f53bbe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57dd2b8b-b56e-4a65-8d38-47c4eab999c0",
                    "LayerId": "fcf34e9a-4d00-4249-bae5-19469fea6832"
                }
            ]
        },
        {
            "id": "9925b2d4-8521-44f0-9911-2fe87a11612b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1fbbe74d-5936-47a5-ac27-b677488dc985",
            "compositeImage": {
                "id": "074f0e15-ac80-417b-8724-b72d454c8300",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9925b2d4-8521-44f0-9911-2fe87a11612b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fff50c2b-76cd-4f33-8310-9d981e0c2981",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9925b2d4-8521-44f0-9911-2fe87a11612b",
                    "LayerId": "fcf34e9a-4d00-4249-bae5-19469fea6832"
                }
            ]
        },
        {
            "id": "a22a84d8-4897-432f-be60-1775c54c6ea6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1fbbe74d-5936-47a5-ac27-b677488dc985",
            "compositeImage": {
                "id": "b6adf8fd-8f4b-412b-b225-d55bc5e54dd7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a22a84d8-4897-432f-be60-1775c54c6ea6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f1699d81-8f58-4d66-a966-639d2657bf63",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a22a84d8-4897-432f-be60-1775c54c6ea6",
                    "LayerId": "fcf34e9a-4d00-4249-bae5-19469fea6832"
                }
            ]
        },
        {
            "id": "c2b8e6bd-30d5-49cf-a465-e8ecd939a0b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1fbbe74d-5936-47a5-ac27-b677488dc985",
            "compositeImage": {
                "id": "3aab0cba-ce95-45e9-afe1-e3369f11ad9d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2b8e6bd-30d5-49cf-a465-e8ecd939a0b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a4987ff-6dc9-4852-ae07-13ba54cea229",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2b8e6bd-30d5-49cf-a465-e8ecd939a0b2",
                    "LayerId": "fcf34e9a-4d00-4249-bae5-19469fea6832"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "fcf34e9a-4d00-4249-bae5-19469fea6832",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1fbbe74d-5936-47a5-ac27-b677488dc985",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 128
}