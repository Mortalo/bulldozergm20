{
    "id": "ca1c32c9-e259-41ad-874b-d13faff63ae7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_title",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 59,
    "bbox_left": 4,
    "bbox_right": 464,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8112fdda-ed53-40fd-a906-cbe8424d8237",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ca1c32c9-e259-41ad-874b-d13faff63ae7",
            "compositeImage": {
                "id": "5c4e65a9-4689-4879-9e79-4b54e021576e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8112fdda-ed53-40fd-a906-cbe8424d8237",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9e51456-7378-48b5-b9ba-75260f884e18",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8112fdda-ed53-40fd-a906-cbe8424d8237",
                    "LayerId": "de255a62-3c46-4b8e-bfed-81a2718e4343"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 160,
    "layers": [
        {
            "id": "de255a62-3c46-4b8e-bfed-81a2718e4343",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ca1c32c9-e259-41ad-874b-d13faff63ae7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 500,
    "xorig": 0,
    "yorig": 0
}