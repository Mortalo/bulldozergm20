{
    "id": "adc79eea-4f49-48d1-8142-bf2da9967cab",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_building_gray_3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 159,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 33,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b4130358-4531-4f18-b3eb-489738c66019",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "adc79eea-4f49-48d1-8142-bf2da9967cab",
            "compositeImage": {
                "id": "d9c959bb-ec46-424d-9c36-adf414b08c9f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4130358-4531-4f18-b3eb-489738c66019",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8984c7a-1603-4fac-9ba4-807362946f1f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4130358-4531-4f18-b3eb-489738c66019",
                    "LayerId": "a112aa67-b5bc-4834-95b9-a36a7b371e5c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 160,
    "layers": [
        {
            "id": "a112aa67-b5bc-4834-95b9-a36a7b371e5c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "adc79eea-4f49-48d1-8142-bf2da9967cab",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 96
}