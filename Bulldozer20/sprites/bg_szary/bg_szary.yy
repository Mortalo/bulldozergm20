{
    "id": "3d3bb763-e589-4537-917a-23954ca90e11",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bg_szary",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "671cddf7-19c6-4151-91e1-608dda5be6fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3d3bb763-e589-4537-917a-23954ca90e11",
            "compositeImage": {
                "id": "ea25e8e0-d830-48b1-8d89-e0504a05107f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "671cddf7-19c6-4151-91e1-608dda5be6fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2f682b9-a01f-4674-8ad1-d5f8d89b0892",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "671cddf7-19c6-4151-91e1-608dda5be6fb",
                    "LayerId": "24a8976f-63ef-4f4c-829e-a82fede433b8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "24a8976f-63ef-4f4c-829e-a82fede433b8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3d3bb763-e589-4537-917a-23954ca90e11",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}