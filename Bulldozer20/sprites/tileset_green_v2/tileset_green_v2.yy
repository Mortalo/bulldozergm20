{
    "id": "0a06132f-31ee-4acd-84d5-e21d84346445",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "tileset_green_v2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 128,
    "bbox_right": 767,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2c167908-9779-4531-adce-976224a7ee1e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0a06132f-31ee-4acd-84d5-e21d84346445",
            "compositeImage": {
                "id": "473ddb0a-1b64-4a2d-876b-e79ebf13c0dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c167908-9779-4531-adce-976224a7ee1e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0fcaf548-1ab3-4a06-b511-9337746be261",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c167908-9779-4531-adce-976224a7ee1e",
                    "LayerId": "95340c15-ee4a-449d-a65d-f8321c8fc004"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "95340c15-ee4a-449d-a65d-f8321c8fc004",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0a06132f-31ee-4acd-84d5-e21d84346445",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 768,
    "xorig": 0,
    "yorig": 0
}