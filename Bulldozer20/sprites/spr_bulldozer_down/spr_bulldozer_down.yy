{
    "id": "392bd4bf-42e1-44e1-a5b2-2797ecbec3fe",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bulldozer_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 159,
    "bbox_left": 4,
    "bbox_right": 123,
    "bbox_top": 32,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "64ccea7e-65e5-4afc-9131-d199f19916bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "392bd4bf-42e1-44e1-a5b2-2797ecbec3fe",
            "compositeImage": {
                "id": "6b42a922-066d-4aa3-806e-87371bd3d399",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64ccea7e-65e5-4afc-9131-d199f19916bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "59b5cbac-118e-4e40-b759-035290ccb09a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64ccea7e-65e5-4afc-9131-d199f19916bc",
                    "LayerId": "0edc300a-a8f6-4fb3-a3da-b75ba6cb9727"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 160,
    "layers": [
        {
            "id": "0edc300a-a8f6-4fb3-a3da-b75ba6cb9727",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "392bd4bf-42e1-44e1-a5b2-2797ecbec3fe",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 96
}