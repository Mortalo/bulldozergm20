{
    "id": "4d0607aa-a17d-4240-bee4-ed44f99c329d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bulldozer_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 151,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 32,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0fde8721-fb55-4704-a66f-ac005627d1c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d0607aa-a17d-4240-bee4-ed44f99c329d",
            "compositeImage": {
                "id": "8c855348-1c78-46e9-8dba-7f901e09e487",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0fde8721-fb55-4704-a66f-ac005627d1c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "017a30be-a5b6-488e-806b-6510ca0b127a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0fde8721-fb55-4704-a66f-ac005627d1c1",
                    "LayerId": "8e6c102f-a5fd-49cf-a543-3e618a6ae90e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 160,
    "layers": [
        {
            "id": "8e6c102f-a5fd-49cf-a543-3e618a6ae90e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4d0607aa-a17d-4240-bee4-ed44f99c329d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 96
}