{
    "id": "fdbe0588-cb78-4ec7-86f8-4baa27953a58",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_building_gray_2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 159,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 33,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9c07afd1-3bac-4136-9c86-4c26d512786e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdbe0588-cb78-4ec7-86f8-4baa27953a58",
            "compositeImage": {
                "id": "54fc9c89-505d-4c6e-9a66-217413dd2486",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c07afd1-3bac-4136-9c86-4c26d512786e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eee49fc6-435f-4ff2-8256-58994a3d3561",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c07afd1-3bac-4136-9c86-4c26d512786e",
                    "LayerId": "19e7faea-648f-46ab-843b-a12d34c04d8f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 160,
    "layers": [
        {
            "id": "19e7faea-648f-46ab-843b-a12d34c04d8f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fdbe0588-cb78-4ec7-86f8-4baa27953a58",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 96
}