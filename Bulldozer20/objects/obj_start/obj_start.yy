{
    "id": "73dcc98e-e72e-4fa5-9293-512b3a45ff43",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_start",
    "eventList": [
        {
            "id": "6b61f385-e0f7-4ef5-a390-11f6ac0db035",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "73dcc98e-e72e-4fa5-9293-512b3a45ff43"
        },
        {
            "id": "a19ab889-541f-466e-8efc-4281a9ba9910",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "73dcc98e-e72e-4fa5-9293-512b3a45ff43"
        },
        {
            "id": "8486190c-73f5-4f4d-879d-8857e3da97e1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "73dcc98e-e72e-4fa5-9293-512b3a45ff43"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}