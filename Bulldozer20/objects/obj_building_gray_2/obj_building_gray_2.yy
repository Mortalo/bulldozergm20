{
    "id": "a4260553-73a0-45a2-b831-a594e9db831e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_building_gray_2",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "b4b31982-a5e4-4dfe-a671-51b532611026",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "fdbe0588-cb78-4ec7-86f8-4baa27953a58",
    "visible": true
}