{
    "id": "6d76cb9e-c0d2-4edc-a773-255dbe9e9248",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_building_gray",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "b4b31982-a5e4-4dfe-a671-51b532611026",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "83e4b0f2-89b8-4b2f-9bde-c44e353a7953",
    "visible": true
}