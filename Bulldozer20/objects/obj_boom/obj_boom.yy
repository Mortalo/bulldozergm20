{
    "id": "c273a49b-b5d5-4718-8f1e-3e67e6b90003",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_boom",
    "eventList": [
        {
            "id": "3e6058e4-9a78-434e-a68c-a6379f6f4ef4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "c273a49b-b5d5-4718-8f1e-3e67e6b90003"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1fbbe74d-5936-47a5-ac27-b677488dc985",
    "visible": true
}