{
    "id": "36353b85-27a2-46a7-ac1e-28c2bf41337c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_building_gray_3",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "b4b31982-a5e4-4dfe-a671-51b532611026",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "adc79eea-4f49-48d1-8142-bf2da9967cab",
    "visible": true
}