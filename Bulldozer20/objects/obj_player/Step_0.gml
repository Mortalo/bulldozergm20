x += keyboard_check_pressed(vk_right)*128*place_meeting(x+128,y,obj_building) - keyboard_check_pressed(vk_left)*128*place_meeting(x-128,y,obj_building); /*+ mouse_check_button_pressed(mb_left)*(sign(mouse_x-x)*128)*sign(max(abs(mouse_x-x)-64,0))*sign(max(abs(mouse_x-x)-abs(mouse_y-y),0))*place_meeting(x+sign(mouse_x-x)*128,y,obj_building)*/;
y += keyboard_check_pressed(vk_down)*128*place_meeting(x,y+128,obj_building) - keyboard_check_pressed(vk_up)*128*place_meeting(x,y-128,obj_building);     /*+ mouse_check_button_pressed(mb_left)*(sign(mouse_y-y)*128)*sign(max(abs(mouse_y-y)-64,0))*sign(max(abs(mouse_y-y)-abs(mouse_x-xprevious),0))*place_meeting(x,y+sign(mouse_y-y)*128,obj_building)*/;
sprite_index = (keyboard_check_pressed(vk_right))*spr_bulldozer_right + (keyboard_check_pressed(vk_left))*spr_bulldozer_left + (keyboard_check_pressed(vk_down))*spr_bulldozer_down + (keyboard_check_pressed(vk_up))*spr_bulldozer_up + sprite_index-sprite_index*(keyboard_check_pressed(vk_right) + keyboard_check_pressed(vk_left) + keyboard_check_pressed(vk_down) + keyboard_check_pressed(vk_up));
instance_destroy(instance_place(x,y,obj_building));
if(!instance_exists(obj_building) && alarm[0] < 0)
	alarm[0] = room_speed;
if(keyboard_check_pressed(vk_escape))
	room_goto(rm_start);
if(keyboard_check_pressed(vk_space) || keyboard_check_pressed(ord("R")))
	room_restart();